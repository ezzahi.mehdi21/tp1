# TP 2

## I. Déploiement simple

Voici le fichier Vagrantfile
```
Vagrant.configure("2")do|config|
  config.vm.box="centos/7"

  config.vm.network "private_network", ip: "192.168.2.11/24"

  config.vm.hostname = "mavm"

  config.vbguest.auto_update = false
  
  config.vm.box_check_update = false 
  
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provision "shell", path: "script.sh"

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1024"]
  end
end
```

Le fichier script sh qui permet d'installer vim.
```
sudo yum install vim
```

## II. Re-package

Il suffit d'aller sur une vm et de faire
```
sudo yum update
sudo yum install vim
sudo yum install epel-release
sudo yum install nginx
sudo firewalld
```
Pour desactiver SELinux il faut modificer le fichier /etc/selinux/config pour le désactiver après reboot, de façon permanente, dans le fichier, remplacer enforcing par permissive.


Création de b2-tp2-centos:
```
PS C:\Users\ezzah\vagrant> vagrant package --output centos7-custom.box
==> default: Exporting VM...
==> default: Compressing package to: C:/Users/ezzah/vagrant/centos7-custom.box
PS C:\Users\ezzah\vagrant> vagrant box add b2-tp2-centos centos7-custom.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'b2-tp2-centos' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/ezzah/vagrant/centos7-custom.box
    box:
==> box: Successfully added box 'b2-tp2-centos' (v0) for 'virtualbox'!
```

## III.

```
Vagrant.configure("2")do|config|
  config.vm.box="b2-tp2-centos"

  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vbguest.auto_update = false
  
  config.vm.box_check_update = false

  config.vm.provision "shell", path: "script.sh"


  config.vm.define "node1" do |node1|

    node1.vm.network "private_network", ip: "192.168.2.21"
    config.vm.hostname = "node1.tp2.b2"
    config.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "1024"]
    end
  end

  config.vm.define "node2" do |node2|

    node1.vm.network "private_network", ip: "192.168.2.22"
    config.vm.hostname = "node2.tp2.b2"
    config.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "512"]
    end
  end


end
```

```
[vagrant@node1 ~]$ curl -kL 192.168.2.22
<h1>Hello from site 1</h1>
```

## IV. Automation here we (slowly) come
Voila le dossier vagrantfile:

```
Vagrant.configure("2")do|config|
  config.vm.box="b2-tp2-centos"

  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vbguest.auto_update = false
  
  config.vm.box_check_update = false

  config.vm.provision "shell", path: "script.sh"


  config.vm.define "node1" do |node1|
    node1.vm.network "private_network", ip: "192.168.2.21"
    node1.vm.hostname = "node1.tp2.b2"
    node1.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "1024"]
    end
  end

  config.vm.define "node2" do |node2|
    node2.vm.network "private_network", ip: "192.168.2.22"
    node2.vm.hostname = "node2.tp2.b2"
    node2.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "512"]
    end
  end


end
```
Le dossier script.sh:
```
useradd admin -m
usermod -aG wheel admin

mkdir /srv/site{1,2}

mkdir /opt/backup/

mv nginx.conf /etc/nginx/
mv backup.sh /opt/backup/

echo "192.168.2.21 node1 node1.tp2.b2" >> /etc/hosts
echo "192.168.2.22 node2 node2.tp2.b2" >> /etc/hosts

useradd web -M -s /sbin/nologin
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=node1.tp2.b2"

mv server.key /etc/pki/tls/private/node1.tp2.b2.key
chmod 400 /etc/pki/tls/private/node1.tp2.b2.key
chown web:web /etc/pki/tls/private/node1.tp2.b2.key

mv server.crt /etc/pki/tls/certs/node1.tp2.b2.crt
chmod 400 /etc/pki/tls/certs/node1.tp2.b2.crt
chown web:web /etc/pki/tls/certs/node1.tp2.b2.crt

touch /srv/site1/index.html
touch /srv/site2/index.html

echo "<h1>Hello from site 1</h1>" >> /srv/site1/index.html
echo "<h1>Hello from site 2</h1>" >> /srv/site2/index.html
chown web:web /srv/site1 -R
chmod 700 /srv/site1 /srv/site2
chmod 400 /srv/site1/index.html /srv/site2/index.html

systemctl start firewalld

systemctl start nginx

groupadd backup
groupadd web
useradd backup -M -s /sbin/nologin
usermod -aG backup backup
usermod -aG backup web
usermod -aG web web


chmod 700 /opt/backup
chmod 500 /opt/backup/tp1_backup.sh
chmod 550 /srv/


echo "30 * * * * backup /opt/tp2_backup.sh /srv/site1" >> /etc/crontab 
echo "30 * * * * backup /opt/tp2_backup.sh /srv/site1" >> /etc/crontab 
```
Pour le script de sauvegarde et pour le fichier nginx.conf je les ai recopié à partir de la correction et mis dans le dossier racine pour faire mes essais. (je t'allege le rendu en ne les mettant pas).



```
[vagrant@node1 ~]$ curl -kL 192.168.2.22
<h1>Hello from site 2</h1>
```






