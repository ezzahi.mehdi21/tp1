#!/bin/sh
#Mehdi
#22/10/2020
#Script install gitea

echo "192.168.4.12  mariadb.tp4.b2 mariadb" >> /etc/hosts
echo "192.168.4.13  nginx.tp4.b2 nginx" >> /etc/hosts
echo "192.168.4.14  nfs.tp4.b2 nfs" >> /etc/hosts

yum install -y wget
yum install -y git

firewall-cmd --add-port=3000/tcp --permanent
firewall-cmd --reload

wget -O gitea https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64
chmod +x gitea

adduser --system --shell /bin/bash --comment 'Git Version Control' --user-group --create-home git
passwd -f -u git

mkdir -p /var/lib/gitea/{custom,data,log}
chown -R git:git /var/lib/gitea/
chmod -R 750 /var/lib/gitea/
mkdir /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea

# création du service gitea (sans les commentaires)
echo "[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target
[Service]
RestartSec=2s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
ExecStart=/usr/local/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea
[Install]
WantedBy=multi-user.target" > /etc/systemd/system/gitea.service

systemctl daemon-reload
systemctl enable gitea
systemctl start gitea

yum install -y nfs-utils
mkdir /mnt/gitea
mount 192.168.4.14:/home/vagrant/gitea /mnt/gitea


bash <(curl -Ss https://my-netdata.io/kickstart.sh)
echo 'SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/765562043369127968/f8LCfSkyBsmagNzfav3LS6y-4q9c9L9oO6lxZEhr1z4SwFX5tzbozPk6L6uIqLgm5gbk"
DEFAULT_RECIPIENT_DISCORD="alarms"' > /etc/netdata/health_alarm_notify.conf