#!/bin/sh
#Mehdi
#22/10/2020
#Script install nfs

echo "192.168.4.11  gitea.tp4.b2 gitea" >> /etc/hosts
echo "192.168.4.12  mariadb.tp4.b2 mariadb" >> /etc/hosts
echo "192.168.4.13  nginx.tp4.b2 nginx" >> /etc/hosts

systemctl start nfs-server rpcbind
systemctl enable nfs-server rpcbind

mkdir /nfsbackup/

mkdir /nfsbackup/gitea/
mkdir /nfsbackup/mariadb/
mkdir /nfsbackup/nginx/

chmod 777 /nfsbackup/


echo -e "
/nfsbackup/gitea 192.168.4.11(rw,sync,no_root_squash)
/nfsbackup/mariadb 192.168.4.12(rw,sync,no_root_squash)
/nfsbackup/nginx 192.168.4.13(rw,sync,no_root_squash)
" > /etc/exports

firewall-cmd --permanent --add-service mountd
firewall-cmd --permanent --add-service rpc-bind
firewall-cmd --permanent --add-service nfs
firewall-cmd --reload

bash <(curl -Ss https://my-netdata.io/kickstart.sh)
echo 'SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/765562043369127968/f8LCfSkyBsmagNzfav3LS6y-4q9c9L9oO6lxZEhr1z4SwFX5tzbozPk6L6uIqLgm5gbk"
DEFAULT_RECIPIENT_DISCORD="alarms"' > /etc/netdata/health_alarm_notify.conf