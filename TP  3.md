# TP  3

```
[vagrant@node1 ~]$ sudo systemctl list-unit-files -t service -a | wc -l
157
```
```
[vagrant@node1 ~]$ sudo systemctl -t service | wc -l
43
```
```
[vagrant@node1 ~]$ sudo systemctl -t service | grep "failed\|exited" | wc -l
17
```
```
[vagrant@node1 ~]$ sudo systemctl list-unit-files -t service | grep "enabled" | wc -l
30
```

## 2
Grâce à la commande `systemctl status nginx`
```
/usr/lib/systemd/system/nginx.service
```
**ExecStart** permet d'indiquer la commande à exécuter au lancement du service. Ce paramètre est obligatoire pour tout les types de service.
**PIDFile** indique où trouver l'identifiant PID du processus du service
**Type** permet de specifier le type de service
**ExecReload** permet de redemarer le service
**Description** permet de donner une description du service qui apparaîtra lors de l'utilisation de la commande systemctl status <nom_du_service>
**After** permet d'indiquer quel pré-requis est nécessaire pour le fonctionnement du service. 
**ExecStartPre** spécifie spécifie des commandes personnalisées à exécuter avant

Listez les services qui contiennent la ligne WantedBy=multi-user.target :

```[vagrant@node1 ~]$ grep -r "WantedBy=multi-user.target" /usr/lib/systemd/system/```

## 3. Création d'un service
### A Serveur web
```
[Unit]
Description=Serveur
After=network-online.target

 
[Service]
Type=simple
User=web
Environment=Port=800
ExecStartPre=/usr/bin/firewall-cmd --zone=public --permanent --add-port=$Port/tcp
ExecStopPost=/usr/bin/firewall-cmd --zone=public --remove-port=$Port/tcp
ExecStart=/usr/bin/python3 -m http.server $Port 


RemainAfterExit=no
Restart=on-failure
```
### B Sauvegarde
```
[Unit]
Description=Script sauvegarde



[Service]
Type=simple
User=backup
Pidfile=/var/run/script3.pid

ExecStartPre=/usr/bin/bash /srv/script1.sh /srv/site1
ExecStart=/usr/bin/bash /srv/script2.sh /srv/site1
ExecStartPost=/usr/bin/bash /srv/script3.sh /srv/site1



RemainAfterExit=no
Restart=on-failure
[Install]
WantedBy=multi-user.target

```
Les 3 scripts de sauvegardes se trouvent dans un sous dossier du git.

Voici qui prouve que le script marche
```
[backup@node1 ~]$ sudo journalctl -xe
-- Subject: Unit script3.service has finished start-up
-- Defined-By: systemd
-- Support: http://lists.freedesktop.org/mailman/listinfo/systemd-devel
--
-- Unit script3.service has finished starting up.
--
-- The start-up result is done.
Oct 06 15:39:39 node1.tp2.b2 polkitd[370]: Unregistered Authentication Agent for unix-process:6216:9603197 (system bus name :1.577, object path /org/freedesktop/
Oct 06 15:39:39 node1.tp2.b2 sudo[6214]: pam_unix(sudo:session): session closed for user root
Oct 06 15:39:39 node1.tp2.b2 bash[6227]: [Oct 06 15:39:39] [INFO] Success. Backup site1_201006_153939.tar.gz has been saved to /opt/backup/.
```
Voila le fichier timer
```
[Unit]
Description=timer
[Timer]
OnCalendar=daily
AccuracySec=1h
Persistent=true
[Install]
WantedBy=timers.target
```
# II. Autres features

Installation de centos 8
```
[vagrant@mavm ~]$ hostnamectl
   Static hostname: mavm
         Icon name: computer-vm
           Chassis: vm
        Machine ID: b823b85e0beb46c79648ac7ce4d68857
           Boot ID: f9464e3659a94a24b0f5288c54c24662
    Virtualization: oracle
  Operating System: CentOS Linux 8 (Core)
       CPE OS Name: cpe:/o:centos:centos:8
            Kernel: Linux 4.18.0-80.el8.x86_64
      Architecture: x86-64
```
Les services les plus lents
```
chronyd.service (499ms)
sshd-keygen@ecdsa.service (763ms)
ldconfig.service (523ms)
```
Modifier fuseau horaire
```
timedatectl set-timezone Europe/Paris
```
Modifier hostname
```
sudo hostnamectl set-hostname mehdi
```