# TP1 : Déploiement classique

## 0. Prérequis

### Partitionnement

```
[root@localhost ~]# sudo lvs
  LV    VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root  centos -wi-ao----  <6.20g
  swap  centos -wi-ao---- 820.00m
  data1 data   -wi-a-----   2.00g
  data2 data   -wi-a-----  <3.00g
 ```
 J'ai ajouté le disque en tant que PV (Physical Volume) dans LVM puis créé un VG pour ensuite créé des LV
 
 ```
 [root@localhost ~]# sudo umount /srv/data1
[root@localhost ~]# sudo mount -av
/                        : ignored
/boot                    : already mounted
swap                     : ignored
mount: /srv/data1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/srv/data1               : successfully mounted
/srv/data2               : already mounted
[root@localhost ~]# sudo reboot
Connection to 192.168.56.101 closed by remote host.
Connection to 192.168.56.101 closed.
```
J'ai formaté data1 et data2 puis j'ai monté leur partition pour ensuite definir un montage automatique au boot de la machine.

### Accés internet
```
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:90:1c:d8 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 86018sec preferred_lft 86018sec
    inet6 fe80::6096:514b:fbde:c1c1/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:ae:31:a5 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.101/24 brd 192.168.56.255 scope global noprefixroute dynamic enp0s8
       valid_lft 472sec preferred_lft 472sec
    inet6 fe80::218b:756b:f54d:ac3c/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[root@localhost ~]# ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.101 metric 101
```

### les machines doivent avoir un nom

```
[root@node1 ~]# sudo vim /etc/hostname
[root@node1 ~]# hostname node2.tp1.b1
```
```
[root@node1 ~]# hostname
node1.tp1.b2
```
```
[root@node2 ~]# hostname
node2.tp1.b2
```

### les machines doivent pouvoir se joindre par leurs noms respectifs
```
[root@node2 ~]# ping node1
PING node1.tp1.b2 (192.168.1.11) 56(84) bytes of data.
```

### un utilisateur administrateur est créé sur les deux machines

```
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL
newuser ALL=(ALL)       ALL
```

### le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires

Il n'y a que ssh qui est ouvert.
```
[newuser@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Setup serveur Web

### Installer le serveur web NGINX 

Instalation de nginx
```
[newuser@node1 ~]$ nginx -v
nginx version: nginx/1.16.1
```

J'ai créer les dossier, ainsi que index, pour le tp j'ai créé un nouveau groupe et un nouvel utilisateur et je leur ai donné les droits du fichier.
```
[root@node1 ~]# chown usertp:grouptp /srv/site1/
[root@node1 ~]# chown usertp:grouptp /srv/site2/
[root@node1 ~]# ls -al /srv/site1/
total 0
drwxr-xr-x. 2 usertp grouptp 24 Sep 24 19:42 .
```
Je modifie les droits
```
[root@node1 ~]# sudo chmod 400 /srv/site1
[root@node1 ~]# sudo chmod 400 /srv/site2
```

J'ai modifié le fichier:
```
[root@node1 ~]# vim /etc/nginx/nginx.conf
[root@node1 ~]# cat /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    server {
        listen 80;
        server_name node1.tp1.b2;

        location / {
                return 301 /site1;
        }

        location /site1 {
                alias /srv/site1;
        }

        location /site2 {
        alias /srv/site2;
        }
    
    }
```

```
[root@node1 ~]# sudo firewall-cmd --add-port=80/tcp --permanent
success
[root@node1 ~]# sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

```

Prouver que la machine node2 peut joindre les deux sites web.

```
[root@node2 network-scripts]# curl 192.168.56.101 -L
<p>Site 1 texte<p>
```
## II. Script de sauvegarde
```
#!/bin/bash

# Ezzahi Mehdi
# 28/09/2020
# Backup script

# date du jour
backupdate=$(date +%Y-%m-%d)

#fichier a sauvegarder
fichier="${1}"

# dossier ou doit se trouver la sauvegarde
dossier="/srv/backup/"$fichier"_"$backupdate".tar.gz"

#compression
tar zcvf $fichier $dossier

# Compte le nombre de dossier

nbr_site="find $dossier  -maxdepth 1 -type f | wc -l | awk '{print $1}'"

# Supprime les fichiers au dessus de 7
if [ "$nbr_site" > 7 ]; then
        rm ls -t ~/ThrashDir/ | tail -n +7
        return 1

fi

```

Contrab
```
[backup@node1 ~]$ crontab -l
```

### III. 

```
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```