Vagrant.configure("2") do |config|
  config.vm.box = "centos/8"

  config.vm.box_version = "1905.1"

  config.vm.network "private_network", ip: "192.168.2.11/24"

  config.vm.hostname = "mavm"

  config.vbguest.auto_update = false
  
  config.vm.box_check_update = false 
  
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provision "shell", path: "script.sh"

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1024"]
  end


end
